---
title: "Redes"
---

# Redes

A la [guia de l'assignatura](http://github.com/matcornic/hugo-theme-learn) podeu veure els horaris, la planificació del temari i la manera en que avaluem.


{{% notice tip %}}
Una **red** es un conjunto de ordenadores conectados entre si, que pueden comunicarse para compartir datos y recursos sin importar la localización física de los distintos dispositivos.
{{% /notice %}}


## Fonctionnalités principales

* [Recherche automatique]({{%relref "UF1/prueba/_index.md#activer-recherche" %}})
* [Mode multi-langue]({{%relref "cont/i18n/_index.md" %}})
* **Nombre de niveau infini dans le menu**
* [List child pages]({{%relref "UF1/components/_index.md" %}})

* **Boutons suivant/précédent automatiquement générés pour naviguer entre les items du menu**
* 






{{% notice info %}}
No se puede hacer una tortilla sin romper unos huevos 
{{% /notice %}}

## Documentation website
This current documentation has been statically generated with Hugo with a simple command : `hugo -t hugo-theme-learn` -- source code is [available here at GitHub](https://github.com/matcornic/hugo-theme-learn)

{{% notice note %}}
Automatically published and hosted thanks to [Netlify](https://www.netlify.com/). Read more about [Automated HUGO deployments with Netlify](https://www.netlify.com/blog/2015/07/30/hosting-hugo-on-netlifyinsanely-fast-deploys/)
{{% /notice %}}