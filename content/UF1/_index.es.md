---
title: Introducción a las redes
weight: 1
pre: "<b>1. </b>"
chapter: true
---

### UF1 

# Introducción a las redes

En aquest tema veurem els principis bàsics que regeixen les xarxes, els components que hi ha en una xarxa i els diferents protocols que gestionen les comunicacions.