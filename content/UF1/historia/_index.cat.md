---
title: Historia
weight: 1
---

{{% notice note %}}
Una red es un conjunto de ordenadores conectados entre si, que pueden comunicarse para compartir datos y recursos sin importar la localización física de los distintos dispositivos. 
{{% /notice %}}

A través de una red se pueden ejecutar procesos en otro ordenador o acceder a sus ficheros, enviar mensajes, compartir programas...

El origen de las redes hay que buscarlo en la Universidad de Hawai, donde se desarrollo, en los años setenta, el Método de Acceso Múltiple con Detección de Portadora y Detección de Colisiones, CSMA/CD (Carrier Sense and Multiple Access with Collition Detection), utilizado actualmente por Ethernet.

Este método surgió ante la necesidad de implementar en las islas Hawai un sistema de comunicaciones basado en la transmisión de datos por radio, que se llamó Aloha, y permite que todos los dispositivos puedan acceder al mismo medio, aunque sólo puede existir un único emisor en cada instante. Con ello todos los sistemas pueden actuar como receptores de forma simultánea, pero la información debe ser transmitida por turnos.

El centro de investigaciones PARC (Palo Alto Research Center) de la Xerox Corporation desarrolló el primer sistema Ethernet experimental en los años 70, que posteriormente sirvió como base de la especificación 802.3 publicada en 1980 por el Institute of Electrical and Electronic Engineers (IEEE).

Se entiende por protocolo el conjunto de normas o reglas necesarios para poder establecer la comunicación entre los ordenadores o hosts de una red. Un protocolo puede descomponerse en niveles lógicos o capas denominados layers. El comité 802 del IEEE (Institute of Electrical and Electronic Engineers) es el encargado del desarrollo de los protocolos estandares basados en el modelo de referencia ISO(International Standards Organization.