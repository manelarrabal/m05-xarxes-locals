+++
title = "Classificació de les xarxes"
date =  2021-10-31T13:52:22-01:00
weight = 5
+++

Una xarxa està composta per molts nodes que estan interconnectats entre ells. Cal recordar, que cada node té una **adreça IP** (etiqueta numèrica) que l'identifica de manera lògica. 


> + Una adreça IP consisteix en 4 octets de 8 bits cada un. Cada bit pot ser un 0 o un 1.
> + Una adreça IP també la podem passar a decimal i estarà composta per 4 octets entre [0-255].
> + Podem fer l'equivalència: 11000000.10101000.00000000.00000001 == 192.168.0.1

#### Classes d'adreça IP privades i les seves característiques

Les adreces les podem classificar per classe **A**, classe **B** i classe **C**.  Segons la classe en la qual pertanyi l'adreça, tindrem uns bits per fer xarxes i uns bits per poder connectar nodes. 

> + Classe A: 10.0.0.0 a 10.255.255.255 (8 bits de xarxa, 24 bits per nodes)
> + Classe B: 172.16.0.0 a 172.31.255.255 (16 bits de xarxa, 16 bits per nodes)
> + Classe C: 192.168.0.0 a 192.168.255.255 (24 bits de xarxa, 8 bits per nodes)


![Classes privades](https://www.networkel.com/wp-content/uploads/2016/12/subnet-mask-classes.png)


