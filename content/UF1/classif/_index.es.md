+++
title = "Classificación de las redes"
date =  2021-10-31T13:52:25-01:00
weight = 5
+++
Una xarxa està composta per molts nodes que estan interconnectats entre ells. Cal recordar, que cada node té una **adreça IP** (etiqueta numèrica) que l'identifica de manera lògica. 


> + Una adreça IP consisteix en 4 octets de 8 bits cada un. Cada bit pot ser un 0 o un 1.
> + Una adreça IP també la podem passar a decimal i estarà composta per 4 octets entre [0-255].
> + Podem fer l'equivalència: 11000000.10101000.00000000.00000001 == 192.168.0.1

#### Classes d'adreça IP privades i les seves característiques

Les adreces les podem classificar per classe **A**, classe **B** i classe **C**.  Segons la classe en la qual pertanyi l'adreça, tindrem uns bits per fer xarxes i uns bits per poder connectar nodes. 

> + Classe A: 10.0.0.0 a 10.255.255.255 (8 bits de xarxa, 24 bits per nodes)
> + Classe B: 172.16.0.0 a 172.31.255.255 (16 bits de xarxa, 16 bits per nodes)
> + Classe C: 192.168.0.0 a 192.168.255.255 (24 bits de xarxa, 8 bits per nodes)


![Classes privades](https://www.bogotobogo.com/DevOps/AWS/images/VPC/IP-ClassABC.png?classes=border,shadow)


Les adreces de classe A normalment són per empreses molt grans que necessiten tenir molt poques xarxes i molts nodes connectats (16,777,214). 

Les adreces de classe B normalment son per empreses mitjanes, podem connectar 65534 nodes.

Les adreces de classe C normalment són utilitzades per empreses petites o entorns domèstics, podem connectar 254 nodes. 

I d'on surten aquests números?  Fem l'exemple per la classe C

2^24 bits = 2,097,152 xarxes diferents. 
2^8 bits = 256 nodes ( - 2 adreces reservades per adreça de xarxa i adreça de broadcast).


#### Com puc saber si una adreça privada és de classe A, B o C? 

Si passem l'adreça de decimal a binari, ho podem saber perquè: 
+ Adreces de **classe A** comencen sempre amb **0XXXXXXX.XXXXXXXX.XXXXXXXX.XXXXXXXX**
+ Adreces de **classe B** comencen sempre amb **10XXXXXX.XXXXXXXX.XXXXXXXX.XXXXXXXX**
+ Adreces de **classe C** comencen sempre amb **110XXXXX.XXXXXXXX.XXXXXXXX.XXXXXXXX**



{{% notice info %}} Exemples de com podem esbrinar el tipus de classe 
{{% /notice %}}

##### Exemple  De quina classe és l'adreça IP 172.16.254.48?

1. Agafem els primers valors que defineixen la classe: 172.
2. Ho passem a binari: 172 == 10101100
3. L’adreça començar per 10XXXXXX, per tant, és una adreça de classe B.

##### Exemple  De quina classe és l'adreça IP 192.32.31.1? 

1. Agafem els primers valors que defineixen la classe: 192.
2. Ho passem a binari: 192== 11000000
3. L’adreça començar per 110XXXXX, per tant, és una adreça de classe C.

##### Exemple  De quina classe és l'adreça IP 34.1.1.1? 
1. Agafem els primers valors que defineixen la classe: 34.
2. Ho passem a binari: 192== 00100010
3. L’adreça començar per 0XXXXXXX, per tant, és una adreça de classe A.







|Pas| Exemple  De quina classe és l'adreça IP 172.16.254.48? |
|-| ----------- |
|1| Agafem els primers valors que defineixen la classe: 172. |
|2| engine to be used for processing templates. Handlebars is the default. |
|3| extension to be used for dest files. 
