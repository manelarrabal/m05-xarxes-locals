+++
title = "Components de xarxa"
date =  2021-10-31T13:11:35-01:00
+++

El concepte de **LAN** és l’acrònim de **Local Area Network**

Una LAN és una interconnexió de diferents ordinadors i perifèrics i per tant permet que dues o més màquines es comuniquin i intercanviïn informació.


Per compondre la LAN tenim 5 categories principals:


{{%children%}}






<!-- markdownlint-disable MD004
+ Nodes (Host)
+ Perifèrics compartits
+ Medis de xarxa
+ Protocols i regles
  markdownlint-enable MD004 -->
